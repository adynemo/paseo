Fixed:
 - New Year's Day bug - Jan 1 no longer considered to be first day of week (unless it actually is).  Number of steps shown for first and last week of year should now be correct.