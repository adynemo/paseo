package ca.chancehorizon.paseo.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.content.ContextCompat
import ca.chancehorizon.paseo.BuildConfig


// when an update to paseo is installed, restart the step counting service
class UpdateReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null) {
            return
        }

        val serviceIntent = Intent(context, StepCounterService::class.java)
        ContextCompat.startForegroundService(context, serviceIntent)
    }
}
