package ca.chancehorizon.paseo

import android.R.attr
import android.content.*
import android.os.Bundle
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.view.ContextThemeWrapper
import ca.chancehorizon.paseo.databinding.FragmentRecordsBinding
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.view.*
import android.widget.PopupMenu
import androidx.core.content.ContextCompat





class RecordsFragment : androidx.fragment.app.Fragment() {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var fragmentRecordsBinding: FragmentRecordsBinding? = null


    // receiver for step counting service
    private var receiver: BroadcastReceiver? = null

    private lateinit var paseoDBHelper : PaseoDBHelper

    private lateinit var contextThemeWrapper : ContextThemeWrapper

    var recordsTimePeriod = "All Time"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // set up the link between this screen and the step counting service
        configureReceiver()

    }



    // set up receiving messages from the step counter service
    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
            }
        }

        context?.registerReceiver(receiver, filter)
    }


    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        fragmentRecordsBinding = null

        context?.unregisterReceiver(receiver)
        super.onDestroy()
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()

        updateRecords()
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment

        val theTheme = requireContext().theme
        contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view : View = inflater.inflate(R.layout.fragment_records, container, false)

        val binding = FragmentRecordsBinding.bind(view)
        fragmentRecordsBinding = binding

        // respond to the user tapping on the hour record steps row
        binding.maxHourRow.setOnClickListener {
            showMoreHighest("hour")
        }

        // respond to the user tapping on the day record steps row
        binding.maxDayRow.setOnClickListener {
            showMoreHighest("day")
        }

        // respond to the user tapping on the week record steps row
        binding.maxWeekRow.setOnClickListener {
            showMoreHighest("week")
        }

        // respond to the user tapping on the month record steps row
        binding.maxMonthRow.setOnClickListener {
            showMoreHighest("month")
        }

        // respond to the user tapping on the year record steps row
        binding.maxYearRow.setOnClickListener {
            showMoreHighest("year")
        }

        // set up the popup menu so that the user can select the time frame for displaying record number of steps
        binding.recordsTimeButton.setOnClickListener {
            // Initializing the popup menu and giving the reference as current context
            val popupMenu = PopupMenu(context, binding.recordsTimeButton)

            // Inflating popup menu from popup_menu.xml file
            popupMenu.getMenuInflater().inflate(R.menu.records_time_menu, popupMenu.getMenu())
            popupMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(menuItem: MenuItem): Boolean {

                    // recordsTimePeriod is use when getting the max steps from the datebase
                    //  (max steps for a particular time period rather than all time)
                    // this is done so that we do not have to handle all locale text versions in the SQL
                    when (menuItem.title) {
                        getResources().getString(R.string.day) -> {
                            val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                            binding.maximumsTitle.text = getString(R.string.maximums).plus(" (").plus(sdf.format(Date()) + ")")
                            recordsTimePeriod = "Day"
                        }
                        getResources().getString(R.string.month) -> {
                            val sdf = SimpleDateFormat("MMM, yyyy", Locale.getDefault())
                            binding.maximumsTitle.text = getString(R.string.maximums).plus(" (").plus(sdf.format(Date()) + ")")
                            recordsTimePeriod = "Month"
                        }
                        getResources().getString(R.string.year) -> {
                            val sdf = SimpleDateFormat("yyyy", Locale.getDefault())
                            binding.maximumsTitle.text = getString(R.string.maximums).plus(" (").plus(sdf.format(Date()) + ")")
                            recordsTimePeriod = "Year"
                        }
                        else -> {
                            recordsTimePeriod = "All Time"
                        }
                    }

                    binding.recordsTimeButton.text = menuItem.title

                    updateRecords()

                    return true
                }
            })
            // Showing the popup menu
            popupMenu.show()
        }

        return view
    }



    // update the list of record number of steps shown on the screen
    fun updateRecords() {

        // first clear all contents of the table
        fragmentRecordsBinding?.maximumsTable?.removeAllViews()

        // number of records to be shown for each time unit (maybe user override in future version)
        val numHighest = 10
        // the row in the table to start inserting new rows
        var startRow = 1

        // update records by time unit
        var maxStepsArray = paseoDBHelper.getMaxSteps("hours", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), numRecords = numHighest, recordsTimePeriod = recordsTimePeriod)

        fillRecordsTable(maxStepsArray,"hours", startRow)
        startRow = startRow + maxStepsArray.size

        maxStepsArray = paseoDBHelper.getMaxSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), numRecords = numHighest, recordsTimePeriod = recordsTimePeriod)
        fillRecordsTable(maxStepsArray,"days", startRow)
        startRow = startRow + maxStepsArray.size

        maxStepsArray = paseoDBHelper.getMaxSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), numRecords = numHighest, recordsTimePeriod = recordsTimePeriod)
        fillRecordsTable(maxStepsArray,"weeks", startRow)
        startRow = startRow + maxStepsArray.size

        maxStepsArray = paseoDBHelper.getMaxSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), numRecords = numHighest, recordsTimePeriod = recordsTimePeriod)
        fillRecordsTable(maxStepsArray,"months", startRow)
        startRow = startRow + maxStepsArray.size

        maxStepsArray = paseoDBHelper.getMaxSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), numRecords = numHighest, recordsTimePeriod = recordsTimePeriod)
        fillRecordsTable(maxStepsArray,"years", startRow)
    }



    // fill in records table for specific time unit
    fun fillRecordsTable(maxStepsArray: ArrayList<Pair <Int, String>>, timeUnit: String, startRow: Int = 0) {

        var dateFormat = "MMM dd, yyyy"

        // the title (text in right most cell) of the first row for a specific time unit
        var rowTitle = "hours"

        // icon for row title
        var rowTitleIcon = R.drawable.ic_menu_hour

        // makw sure to only start inserting rows if the startRow makes sense and there are values in the maxStepsArray
        if (startRow > 0 && maxStepsArray.size > 0) {

            when (timeUnit) {
                "hours" -> {
                    rowTitle = getResources().getString(R.string.hour)
                    rowTitleIcon = R.drawable.ic_menu_hour
                }
                "days" -> {
                    rowTitle = getResources().getString(R.string.day)
                    rowTitleIcon = R.drawable.ic_day
                }
                "weeks" -> {
                    rowTitle = getResources().getString(R.string.week)
                    rowTitleIcon = R.drawable.ic_week
                }
                "months" -> {
                    dateFormat = "MMM, yyyy"
                    rowTitle = getResources().getString(R.string.month)
                    rowTitleIcon = R.drawable.ic_month
                }
                "years" -> {
                    dateFormat = "yyyy"
                    rowTitle = getResources().getString(R.string.year)
                    rowTitleIcon = R.drawable.ic_year
                }
            }

            // add a row in the table for each of the highest steps recorded as returned from the database
            for (stepRecord in 0 until maxStepsArray.size) {

                val (maxHour, theDateTime) = maxStepsArray[stepRecord]

                var stepsTextSize = 12f
                var dateTextSize = 10f

                val row = TableRow(context)
                row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT)

                if (stepRecord == 0){
                    // enter the number of steps for this unit of time in the table
                    val stepsCell = TextView(context)
                    stepsCell.apply {
                        layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                                TableRow.LayoutParams.WRAP_CONTENT)
                        text = rowTitle
                        textSize = 15f
                        gravity = Gravity.START

                        // add an icon to the row title
                        val img = ContextCompat.getDrawable(requireView().context, rowTitleIcon)
                        img!!.setBounds(0, 0, 0 + stepsCell.textSize.toInt(), 0
                                + stepsCell.textSize.toInt())
                        // display the icon in the theme color
                        img.setTint(resolveColorAttr(requireView().context, attr.colorPrimary))
                        setCompoundDrawables(img, null, null, null)
                        // put a little bit of space between the icon and the text
                        setCompoundDrawablePadding(5)
                    }
                    // respond to the user tapping on the hour record steps row
                    row.setOnClickListener {
                        showMoreHighest(rowTitle)
                    }

                    row.setPadding(0,15,0,0)

                    row.addView(stepsCell)
                    stepsTextSize = 20f
                    dateTextSize = 15f
                }
                else {
                    // add an empty cell to the table row
                    val emptyCell = TextView(context)

                    row.addView(emptyCell)
                }

                // enter the number of steps for this unit of time in the table
                val stepsCell = TextView(context)
                stepsCell.apply {
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT)
                    text = NumberFormat.getIntegerInstance().format(maxHour)
                    textSize = stepsTextSize
                    gravity = Gravity.CENTER
                }
                row.addView(stepsCell)

                // set up the time value for the time cell in the table
                val timeCell = TextView(context)
                timeCell.apply {
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT)

                    // show the date/hour of the records
                    if (timeUnit == "hours") {
                        val theHour = theDateTime.substring(8)
                        text =  theHour.plus("h, ").plus(formatTheDate(theDateTime, dateFormat ))
                    }
                    else if (timeUnit == "weeks") {
                        text = formatTheDate(theDateTime, dateFormat)
                    }
                    else {
                        text = formatTheDate(theDateTime, dateFormat)
                    }
                    textSize = dateTextSize
                    gravity = Gravity.END
                }
                row.addView(timeCell)

                // initially keep the row hidden (can be toggled by user)
                if (stepRecord > 0) {
                    row.visibility = View.GONE
                }

                fragmentRecordsBinding!!.maximumsTable.addView(row)
            }
        }
    }



    // show or hide additional highest steps rows
    fun showMoreHighest(timeUnit : String) {

        var checkRows = false
        var checkTimeUnit = true

        // loop through all the table's rows to find the rows to hide/display
        for (i in 0 until fragmentRecordsBinding!!.maximumsTable.getChildCount()) {
            val child: View = fragmentRecordsBinding!!.maximumsTable.getChildAt(i)
            if (child is TableRow) {
                val row = child

                // check the first cell in the table to determine if it is a row that can be hidden/shown
                //  by checking if it is empty
                val rowChild: View = row.getChildAt(0)
                if (rowChild is TextView) {
                    val theView = rowChild

                    // only show/hide rows for the timeunit tapped by the user
                    if (checkTimeUnit && theView.text == timeUnit) {
                        checkRows = true
                        checkTimeUnit = false
                    }
                    else if (checkRows && theView.text == "") {
                        if (row.visibility == View.VISIBLE) {
                            row.visibility = View.GONE
                        }
                        else {
                            row.visibility = View.VISIBLE
                        }
                    }
                    // when a non empty first cell is found (that is not the timeunit tapped by the user), stop checking rows
                    else if (checkRows) {
                        checkRows = false
                    }
                }
            }
        }
    }



    // convert yyyymmdd in another format
    fun formatTheDate(theDate: String, formatString: String): String {
        val sdf = SimpleDateFormat(formatString, Locale.getDefault())
        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_MONTH, theDate.substring(6,8).toInt())
        cal.set(Calendar.MONTH, theDate.substring(4,6).toInt() - 1)
        cal.set(Calendar.YEAR, theDate.substring(0,4).toInt())

        return sdf.format(cal.time) + "   "
    }



}